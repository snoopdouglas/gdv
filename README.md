# gdv

> **NOTE:**
>
> gdv development has stopped. I'll be releasing a bigger / better tool soon.

Featherweight Godot version manager for Linux/macOS.

Written in bash. Requires `curl`.

## Quickstart

```bash
curl -LO https://gitlab.com/snoopdouglas/gdv/-/raw/main/gdv
chmod +x gdv

./gdv get 3.4
./gdv 3.4
```

This will download gdv, which in turn downloads Godot 3.4 to `~/.gdv/godot-3.4`. It then opens the editor.

## Usage

```bash
gdv [--[no-]mono] SUBCOMMAND
```

* gdv defaults to using regular builds of Godot. For Mono, specify `--mono` as the first argument, or see [configuration](#configuration).
* Regular builds are stored in `~/.gdv`; Mono builds in `~/.gdv/mono`. See [configuration](#configuration) if you want to change this.
* If you're on macOS, `.app` will be appended to all of the Godot filenames shown here, because yknow.

### `gdv VERSION`

Runs Godot with the specified `VERSION` - from `~/.gdv/godot-VERSION` - presuming it's been downloaded.

Extraneous arguments will be passed on to Godot:

```bash
gdv 3.4         # open godot 3.4 editor
gdv 3.3.4 .     # run the project in the current directory on 3.3.4
gdv 3.2.3 -e .  # edit the project in the current directory in 3.2.3

gdv 3.4 --export windows game.exe .  # export the project to 'game.exe'
                                     # using a template named 'windows'
```

### `gdv get VERSION`

Downloads the specified Godot `VERSION` to `~/.gdv/godot-VERSION`.

```bash
gdv get 3.4         # downloads godot 3.4 to ~/.gdv/godot-3.4
gdv --mono get 3.4  # downloads godot 3.4 mono to ~/.gdv/mono/godot-3.4
```

### `gdv check VERSION`

Exits with an error status if the specified `VERSION` hasn't been downloaded - that is, `~/.gdv/godot-VERSION` doesn't exist.

```bash
gdv get 3.4
gdv check 3.4
echo $?  # => 0
gdv check 4.0
echo $?  # => 1
```

### `gdv ensure VERSION`

Runs `gdv get VERSION` if the specified `VERSION` hasn't been downloaded.

If specified, `--mono` will be passed on to `gdv get`.

## Configuration

gdv can be configured with a couple of environment variables:

* `GDV_PATH`: set this to change gdv's directory to something other than `~/.gdv`. Note this must be an absolute path, and Mono builds will still be downloaded to the `mono/` subdirectory.
* `GDV_MONO`: if set, will default gdv to using Mono builds (`--no-mono` can then be used for regular builds).

## License

[MIT](./LICENSE)

Copy-yeah-right © Doug Thompson 2022
